var appModule = angular.module('adminApp', [
        'ui.bootstrap',
        'ui.bootstrap.tpls',
        'ui.router',
        'ngToast',
        'infinite-scroll',
        'ngTagsInput',
        'adminApp.components',
        'adminApp.common'
    ])
    .config(config)
    .controller('AppController', appController);


function appController(AuthService) {

    activate();

    function activate() {
    }
}

config.$inject = ['$httpProvider', '$urlRouterProvider', '$stateProvider'];
function config($httpProvider, $urlRouterProvider, $stateProvider) {
    var appState = {
        name: 'app',
        url: '/',
        controller: 'AppController',
        controllerAs: 'vm',
        template: '<ui-view></ui-view>',
        resolve: {
            token: ['AuthService', function (AuthService) {
                return AuthService.authenticate();
            }]
        }
    };
    var recipesState = {
        name: 'app.recipes',
        url: 'recipes',
        controller: 'RecipesController',
        controllerAs: 'vm',
        templateUrl: 'app/components/recipes/recipes.html'
    };
    var recipeDetailState = {
        name: 'app.recipeDetail',
        url: 'recipes/:slug',
        controller: 'RecipesDetailController',
        controllerAs: 'vm',
        templateUrl: 'app/components/recipes-detail/recipes-detail.html',
        resolve: {
            recipe: ['RecipesService', '$stateParams', function (RecipesService, $stateParams) {
                return RecipesService.get($stateParams.slug)
            }]
        }
    };
    var recipesEditState = {
        name: 'app.recipeEdit',
        url: 'recipes/edit/:slug',
        controller: 'RecipesEditController',
        controllerAs: 'vm',
        templateUrl: 'app/components/recipes-edit/recipes-edit.html',
        resolve: {
            recipe: ['RecipesService', '$stateParams', function (RecipesService, $stateParams) {
                return RecipesService.get($stateParams.slug)
            }]
        }
    };
    var recipesCreateState = {
        name: 'app.recipeCreate',
        url: 'recipes/new',
        controller: 'RecipesNewController',
        controllerAs: 'vm',
        templateUrl: 'app/components/recipes-new/recipes-new.html'
    };

    $urlRouterProvider.otherwise('/recipes');
    $stateProvider.state(appState);
    $stateProvider.state(recipesState);
    $stateProvider.state(recipesCreateState);
    $stateProvider.state(recipeDetailState);
    $stateProvider.state(recipesEditState);


    $httpProvider.interceptors.push('AuthInterceptor');
}