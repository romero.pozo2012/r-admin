angular.module('adminApp.recipes', [])
    .controller('RecipesController', recipesController);

recipesController.$inject = ['ngToast', '$state', '$uibModal', 'RecipesService'];
function recipesController(ngToast, $state, $uibModal, RecipesService) {
    var vm = this;
    var hasMoreItems = true;
    var page = 0;
    var recipeSearch;
    vm.recipes = [];
    vm.recipeSearchSelected = "";
    vm.recipeDelete = recipeDelete;
    vm.search = search;
    vm.toDetail = toDetail;
    vm.searchRecipe = searchRecipe;
    vm.scrollingNext = scrollingNext;
    vm.labels = labels;
    vm.recipeSearch = {};
    recipeSearch = vm.recipeSearch;

    activate();

    function activate() {
        RecipesService.list()
            .then(function (recipes) {
                vm.recipes = recipes;
            });
    }

    function search(query) {
        return RecipesService.search(query)
            .then(function (recipes) {
                return recipes;
            })
    }

    function searchRecipe() {
        if (!vm.recipeSearch.query) return;

        var healthLabels = RecipesService.getLabelsFromTags(vm.recipeSearch.labels);

        RecipesService.search(vm.recipeSearch.query, healthLabels)
            .then(function (recipes) {
                if (recipes.length > 0) {
                    openSearchResultsModal(recipes);
                } else {
                    ngToast.create('No results found for your search: ' + vm.recipeSearch.query);
                }
            });
    }


    function scrollingNext() {
        if (hasMoreItems) {
            page += 1;
            RecipesService.list(page * 10)
                .then(function (recipes) {
                    if (recipes.length < 10) {
                        hasMoreItems = false;
                    }
                    vm.recipes = vm.recipes.concat(recipes);
                });
        } else {
            ngToast.create('No more recipes');
        }
    }

    function recipeDelete(recipe) {
        if (!recipe) return;
        if (!window.confirm('Are you sure?')) return;
        var label = recipe.label;
        RecipesService.remove(recipe._id)
            .then(function (response) {
                activate();
                ngToast.create('Recipe ' + label + ' deleted');
            })
            .catch(function () {
                ngToast.create('Couldnt delete recipe ' + label + ' try again');
            });
    }

    function openSearchResultsModal(recipes) {
        return $uibModal.open({
            animation: true,
            controller: OpenSearchResultsController,
            templateUrl: 'recipeSearchResult.html',
            controllerAs: 'vm',
            size: 'lg',
            resolve: {
                recipes: function () {
                    return recipes;
                }
            }
        });
    }

    function toDetail(slug) {
        $state.go('app.recipeDetail', {slug: slug})
    }

    function labels(){
        return RecipesService.getHealthLabels();
    }

    //Modal Controller
    OpenSearchResultsController.$inject = ['$uibModalInstance', 'RecipesService', 'recipes'];
    function OpenSearchResultsController($uibModalInstance, RecipesService, recipes) {
        var vm = this;
        var page = 0;
        var hasMoreItems = true;
        vm.recipeDelete = recipeDelete;
        vm.closeModal = closeModal;
        vm.next = next;
        vm.scrollingNext = scrollingNext;
        vm.prev = prev;
        vm.recipes = recipes;

        function next() {
            page += 1;
            RecipesService.search(recipeSearch.query, recipeSearch.labels, page * 10)
                .then(function (recipes) {
                    vm.recipes = recipes;
                });
        }

        function scrollingNext() {
            if (hasMoreItems) {
                page += 1;
                RecipesService.search(recipeSearch.query, recipeSearch.labels, page * 10)
                    .then(function (recipes) {
                        if (recipes.length < 10) {
                            hasMoreItems = false;
                        }
                        vm.recipes = vm.recipes.concat(recipes);
                    });
            } else {
                ngToast.create('No more recipes');
            }
        }

        function prev() {
            if (page >= 0) {
                page -= 1;
                RecipesService.search(recipeSearch.query, recipeSearch.labels, page * 10)
                    .then(function (recipes) {
                        vm.recipes = recipes;
                    })
            }
        }

        function closeModal() {
            $uibModalInstance.close();
        }
    }
}