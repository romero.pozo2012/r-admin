angular.module('adminApp.recipesDetail', [])
    .controller('RecipesDetailController', recipesDetailController);

recipesDetailController.$inject = ['recipe', 'RecipesService'];
function recipesDetailController(recipe, RecipesService) {
    var vm = this;
    vm.recipe = {};

    activate();

    function activate() {
        vm.recipe = recipe;
    }

}