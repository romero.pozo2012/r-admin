angular.module('adminApp.recipesEdit', [])
    .controller('RecipesEditController', recipesEditController);

recipesEditController.$inject = ['$scope', '$q','recipe', 'ngToast', 'RecipesService'];
function recipesEditController($scope, $q, recipe, ngToast, RecipesService) {
    var vm = this;
    vm.recipe = {};
    vm.updateRecipe = updateRecipe;
    vm.labels = labels;

    activate();

    function activate() {
        vm.recipe = recipe;

        $scope.$watchCollection(function () {
            return vm.recipe.ingredientLines
        }, function (ingredientLines) {
            var valid = ingredientLines.length > 0;
            $scope.recipeUpdateForm.$setValidity('ingredients', valid);
        }, true);
        $scope.$watchCollection(function () {
            return vm.recipe.digest
        }, function (digest) {
            var valid = digest.length > 0;
            $scope.recipeUpdateForm.$setValidity('digest', valid);
        }, true);
    }

    function labels(){
        return RecipesService.getHealthLabels();
    }

    function updateRecipe(){
        $scope.$broadcast('show-errors-check-validity');
        if ($scope.recipeUpdateForm.$valid) {
            var recipe = angular.copy(vm.recipe);

            recipe.healthLabels = RecipesService.getLabelsFromTags(recipe.healthLabels);

            RecipesService.update(vm.recipe._id, recipe)
                .then(function (response) {
                    //Has to handle error messages due to types
                    ngToast.create('Recipe ' + vm.recipe.label + ' updated');
                    //ngToast.create(JSON.stringify(response));
                    $scope.$broadcast('show-errors-reset');
                })
                .catch(function () {
                    ngToast.create('Couldnt update recipe ' + vm.recipe.label + ' try again');
                });
        }else{
            ngToast.create('Check form errors');
        }
    }
}