angular.module('adminApp.recipesNew', [])
    .controller('RecipesNewController', recipesNewController);

recipesNewController.$inject = ['$scope', 'ngToast', 'RecipesService'];
function recipesNewController($scope, ngToast, RecipesService) {
    var vm = this;
    vm.recipe = {
        ingredientLines: [],
        digest: []
    };
    vm.createRecipe = createRecipe;
    vm.labels = labels;

    activate();

    function activate() {

        $scope.$watchCollection(function () {
            return vm.recipe.ingredientLines
        }, function (ingredientLines) {
            var valid = ingredientLines.length > 0;
            $scope.recipeUpdateForm.$setValidity('ingredients', valid);
        }, true);
        $scope.$watchCollection(function () {
            return vm.recipe.digest
        }, function (digest) {
            var valid = digest.length > 0;
            $scope.recipeUpdateForm.$setValidity('digest', valid);
        }, true);

    }
    function labels(){
        return RecipesService.getHealthLabels();
    }
    function createRecipe() {
        $scope.$broadcast('show-errors-check-validity');
        if ($scope.recipeUpdateForm.$valid) {
            vm.recipe._id = slugify(vm.recipe.label + vm.recipe.provider);
            var recipe = angular.copy(vm.recipe);
            for (var i = 0; i < recipe.healthLabels.length; i++) {
                recipe.healthLabels[i] = recipe.healthLabels[i].text;
            }
            RecipesService.create(recipe)
                .then(function (response) {
                    //Has to handle error messages due to types
                    ngToast.create('Recipe ' + vm.recipe.label + ' created');
                    $scope.$broadcast('show-errors-reset');
                })
                .catch(function () {
                    ngToast.create('Couldnt create recipe ' + vm.recipe.label + ' try again');
                });
        }
    }

    function slugify(text) {
        return text.toString().toLowerCase()
            .replace(/\s+/g, '-')           // Replace spaces with -
            .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
            .replace(/\-\-+/g, '-')         // Replace multiple - with single -
            .replace(/^-+/, '')             // Trim - from start of text
            .replace(/-+$/, '');            // Trim - from end of text
    }
}