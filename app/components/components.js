angular.module('adminApp.components', [
    'adminApp.recipes',
    'adminApp.recipesEdit',
    'adminApp.recipesNew',
    'adminApp.recipesDetail'
]);