angular.module('adminApp.services.auth', [])
    .factory('AuthService', authService)
    .factory('AuthInterceptor', AuthInterceptor);


authService.$inject = ['$window','$q', '$http', '$httpParamSerializerJQLike', 'BASE_URL'];
function authService($window, $q, $http, $httpParamSerializerJQLike, BASE_URL) {
    return {
        authenticate: function () {
            var url = BASE_URL + '/authenticate';
            var defer = $q.defer();
            $http({
                url: url,
                method: 'POST',
                data: $httpParamSerializerJQLike({
                    id: 'test',
                    password: 'welcome'
                }),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then(function (response) {
                if (response.data.success && response.data.token) {
                    $window.localStorage.setItem('authToken', response.data.token);
                    defer.resolve(response.data.token);
                }
            }, function (response) {
                defer.reject(response);
            });

            return defer.promise;
        }
    }
}

AuthInterceptor.$inject = ['$window', '$q'];
function AuthInterceptor($window, $q) {
    return {
        request: function (config) {
            config.headers = config.headers || {};
            if ($window.localStorage.getItem('authToken')) {
                config.headers['x-access-token'] = $window.localStorage.getItem('authToken');
            }
            return config || $q.when(config);
        },
        response: function(response) {
            return response || $q.when(response);
        }
    };
}