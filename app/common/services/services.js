angular.module('adminApp.services', [
    'adminApp.services.auth',
    'adminApp.services.recipes',
    'adminApp.services.constants',
])