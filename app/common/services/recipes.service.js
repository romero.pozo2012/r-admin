angular.module('adminApp.services.recipes', [])
    .factory('RecipesService', recipesService);


recipesService.$inject = ['$q', '$http', '$httpParamSerializerJQLike', 'BASE_URL'];
function recipesService($q, $http, $httpParamSerializerJQLike, BASE_URL) {
    return {
        getHealthLabels: function () {
            var defer = $q.defer();
            defer.resolve(['Vegetarian','Vegan','Paleo','High-Fiber','High-Protein','Low-Carb','Low-Fat','Low-Sodium','Low-Sugar','Alcohol-Free','Balanced']);
            return defer.promise;
        },
        getLabelsFromTags: function (tagObjects) {
            var result = [];
            for (var i = 0; i < tagObjects.length; i++) {
                if(tagObjects[i].text){
                    result.push(tagObjects[i].text.toLowerCase());
                }
            }

            return result;
        },
        list: function (skip) {
            var defer = $q.defer();
            var listUrl = BASE_URL + '/recipes/?limit=10';
            if(skip){
                listUrl = listUrl + '&skip=' + skip;
            }
            $http({
                url: listUrl,
                method: 'GET'
            }).then(function (response) {
                    console.log(response.data);
                    defer.resolve(response.data);
                })
                .catch(function (response) {
                    defer.reject(response)
                });

            return defer.promise;
        },
        create: function (createPayload) {
            var defer = $q.defer();
            $http({
                url: BASE_URL + '/recipe/addNew/',
                method: 'POST',
                data: $httpParamSerializerJQLike(createPayload),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then(function (response) {
                    defer.resolve(response.data);
                })
                .catch(function (response) {
                    defer.reject(response)
                });

            return defer.promise;
        },
        get: function (slug) {
            var defer = $q.defer();
            $http({
                url: BASE_URL + '/recipe/' + slug,
                method: 'GET'
            }).then(function (response) {
                    console.log(response.data);
                    defer.resolve(response.data);
                })
                .catch(function (response) {
                    defer.reject(response)
                });

            return defer.promise;
        },
        update: function (slug, updatePayload) {
            console.log(slug, updatePayload)

            var defer = $q.defer();
            $http({
                url: BASE_URL + '/recipe/' + slug,
                method: 'PUT',
                data: $httpParamSerializerJQLike(updatePayload),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then(function (response) {
                    defer.resolve(response.data);
                })
                .catch(function (response) {
                    defer.reject(response)
                });

            return defer.promise;
        },
        remove: function (slug) {
            var defer = $q.defer();
            $http({
                url: BASE_URL + '/recipe/' + slug,
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then(function (response) {
                    defer.resolve(response.data);
                })
                .catch(function (response) {
                    defer.reject(response)
                });

            return defer.promise;
        },
        search: function (query, labels, skip, limit) {
            var defer = $q.defer();
            var searchUrl = BASE_URL + '/recipes/?q='+ query + '&limit=10';
            if(labels){
                searchUrl = searchUrl + '&diet=' + labels;
            }
            if(skip){
                searchUrl = searchUrl + '&skip=' + skip;
            }
            if(limit){
                searchUrl = searchUrl + '&limit=' + limit;
            }
            $http({
                url: searchUrl,
                method: 'GET'
            }).then(function (response) {
                    defer.resolve(response.data);
                })
                .catch(function (response) {
                    defer.reject(response)
                });

            return defer.promise;
        }
    }
}